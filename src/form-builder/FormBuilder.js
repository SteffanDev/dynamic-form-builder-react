import React from "react";
import ReactDOM from "react-dom";
import { FormBuilder, FormEdit, Form, FormGrid } from "react-formio";
import BgCard from "../atom/background/BgCard";
import { Tab, Tabs, Container } from "react-bootstrap";
import "../scss/formio/formio.form.builder.scss";
import "../scss/formio/formio.form.scss";
import "../scss/Main.scss";
// import "./FormBuilder.scss";
import {
  faPlusCircle,
  faTrashAlt,
  faEdit,
  faKey,
  faGavel
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class ReactFormBuilder extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      schema: [],
      data: [],
      processData: [],
      processId: "",
      taskData: [],
      taskId: "",
      items: [],
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods":
          "GET, POST, PATCH, PUT, DELETE, OPTIONS",
        "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
      }
    };

    this.functions = {};
  }

  onSubmit(submission) {
    // console.log("submission object-->", submission);
  }


  handleChange(event) {
    console.log(event.target.id);
    console.log(event.target.value);
    console.log(event.target.name);

    var name = event.target.name;
    var value = event.target.value;

    this.setState({ [event.target.name]: event.target.value }, () => { });
  }

  componentDidMount() {

  }

  //rendering the preview page onChange
  setFormView(schema) {
    var items = [];
    this.setState({
      items: []
    });

    if (schema.components[1] != undefined) {
      items.push(<Form src={schema} />);
      this.setState({ items: items, schema: schema }, () => { });
    }
    return items;
  }

  render() {

    return (
      <React.Fragment>
        <div className="ui-editor-wrap ">
          {/* <Header /> */}

          <Container fluid={true} className=" ui-editor">
            <BgCard title="UI Builder" titleDisplay="block">
              <Tabs
                className="nav-fill"
                defaultActiveKey="Builder"
                id="uncontrolled-tab-example"
              >
                <Tab eventKey="Builder" title="Builder">
                  <FormBuilder
                    form={{ display: "form" }}
                    onChange={schema => this.setFormView(schema)}
                  />
                </Tab>
                <Tab eventKey="Viewer" title="Viewer">
                  {this.state.items}
                </Tab>
                {/* <Tab eventKey="Editor" title="Editor">
                  <FormEdit
                    src={this.state.schema}
                    // onSubmit={submission =>
                    //   console.log(JSON.stringify(submission))
                    // }
                  />
                </Tab> */}
              </Tabs>
              <br></br>
            </BgCard>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}


export default ReactFormBuilder;
