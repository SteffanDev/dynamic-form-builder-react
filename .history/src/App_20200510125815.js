import React from 'react';
import logo from './logo.svg';
import './App.css';
import FormBuilderFunc from './form-builder/FormBuilder';

function App() {
  return (
    <div className="App">
      <FormBuilderFunc />
    </div>
  );
}

export default App;
