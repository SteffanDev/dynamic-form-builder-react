/**
 * @author : Sameera Peiris
 * @date : September 18, 2019
 * @version : 1.0
 * @copyright : © 2010-2019 Information International Limited. All Rights Reserved
 */

import React from "react";
import PropTypes from "prop-types";
import "./BgCard.scss";

const BgCard = ({ color,children, className, title, titleDisplay }) => {
  return (
      <div className={`life--bgcard-${color} ${className}`}>
     <h4 className={`life--bgcard-title d-${titleDisplay}`}>{title}</h4>
        <div className="life--bgcard-body">
            {children}
        </div>
       
      </div>
  );
};

BgCard.propTypes = {
  title: PropTypes.any,
  className: PropTypes.any
};

BgCard.defaultProps = {
  color: "white",
  titleDisplay: "none"
};
export default BgCard;
