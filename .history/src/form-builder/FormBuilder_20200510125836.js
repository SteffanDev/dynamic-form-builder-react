import React from 'react';
import FormBuilder from 'react-form-builder2';

function FormBuilderFunc() {
    return (
        <React.Fragment>
            <FormBuilder.ReactFormGenerator
                form_action="/path/to/form/submit"
                form_method="POST"
                task_id={12} // Used to submit a hidden variable with the id to the form from the database.
                answer_data={} // Answer data, only used if loading a pre-existing form with values.
                authenticity_token={} // If using Rails and need an auth token to submit form.
                data={} // Question data
            />,
        </React.Fragment>
    )
}

export default FormBuilderFunc;
