import React from 'react';
import logo from './logo.svg';
import './App.css';
import FormBuilder from './form-builder/FormBuilder';

function App() {
  return (
    <div className="App">
      <FormBuilder />
    </div>
  );
}

export default App;
